# IMT3612 Exam - Andreas Wang#

  * [Vulkan](Vulkan) - foviated rendering

## Process
For the exam you will make commit messages per one hour or **work** of the 24 hours. Thus there should be about 8 to 10 commits at a minimum. Do not commit while you are asleep.
The commit messages are also assessable.  

If you get stuck you need to document what you were trying to do and how you were trying to solve problems.

Add Simon as an admin and I will add the exam group.

Make sure you are watching this repository

## Questions and Answers:

**Q:** You mentioned something about performance measurement to get high grades.

**A:**  Well for OpenCL you should be able to time how long the process takes on the GPU and on the CPU and discuss at what point it becomes useful to send processing to the GPU.  This includes the discussion of the time taken to transmit data to the card. For Vulkan you can discuss the performance, and for Unity you do not have to measure the time unless you already have a CPU visual effect.

**Q:** Do I have to implement in both GPU and CPU

**A:**  Not for normal grades. If you want an A it could be one of the things you do to compare performance.


**Q:** Is there language restrictions on the CPU programming

**A:** You can use the language you are most comfortable with. We recommend C++, Java, C, C# or Go

**Q:** Do I need to commit when I have just been reading stuff

**A:** Yes, write the names of the documents or code you are reading into the readme and commit with a message about starting to analyse what needs to be done.

**Q:** How do I add comments to the commit for the readme.md as editing it on the front page just places a default commit message

**A:** You have to go to source, then find the readme.md file, and then edit it as a source file.  That will then give you the <view diff> and <commit> buttons.  The commit button will then allow you to add a commit message even when editing online.  The other option is to edit it on your machine and do a normal commit.

**Q:** What if I do not get it finished, what should I focus on

**A:** You should focus on showing that you have the ability to program the GPU. Show off the ability to put processing into the kernals of the GPU and get results back. You can get a good mark with implementing only some of the task.  Remember there are also a lot of restictions.

**Q** Can we convert the image to png

**A** yes you can convert images to png or jpeg