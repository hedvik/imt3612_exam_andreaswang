## Implementation of foviated rendering in Vulkan
My implementation of foviated rendering is fairly simple at the high level. It uses two render passes:
	
	* The first render pass works by rendering the entire scene to a low resolution framebuffer and copying the results to a texture.
	* The final render pass renders the entire scene again, this time at a high resolution, but replaces any fragments outside of a given radius around the mouse cursor with the low resolution texture instead. This means that only fragments within the radius of the circle perform any additional calculations. 

The code for rendering to the framebuffer and copying the result to a texture is based on Sascha Willems' code sample for offscreen rendering(https://github.com/SaschaWillems/Vulkan/blob/master/offscreen/offscreen.cpp) as well as my shadow mapping code from the Pacman 3D implementation in Vulkan. 
Since my shadow mapping code also was based on code from the repo, cross referencing between the code in this link and my shadow mapping implementation was fairly quick and efficient. 

To take a somewhat more in-depth look at the implementation and go through all the Vulkan necessary components:
The offscreen rendering pass requires its own framebuffer, rendering textures, render pass, command buffer and semaphore for synchronization. On top of this we also need a separate pipeline as we are using separate shaders. 
The framebuffer has a resolution of (width = screenWidth/9, height = screenHeight/9). 
The offscreen command buffer is used to record all of the draw calls and descriptor(uniform) bindings for the render pass. 
Once offscreen rendering is finished, the offscreen semaphore gets signaled which lets the program continue with the next and final render pass. 

The final render pass works similarly to its offscreen counterpart with some minor changes. We are constantly updating the screen position of the mouse cursor and sending it to the shaders.  The fragment shader then tries to render the scene at high resolution, but checks whether the fragment is within a circle provided with a uniform buffer before doing so. The fragment is rendered with all fragment computations if the current fragment is within the radius of the circle. If the current fragment is outside of the circle radius, it is replaced by sampling from the low resolution texture. Finding the current texture coordinates within the texture is handled by calculating (gl_FragCoord.xy / vec2(SCREEN_WIDTH, SCREEN_HEIGHT)).
	
I wouldn't call this a full and proper implementation of foviated rendering though as that would require blurring around the fovea edges and at least two textures with varying quality combined with a final high resolution render of the circular region around the cursor. Regardless it provides most of the necessary behaviour and could be extended with additional render passes. 

## Challenges met during implementation
One of the major challenges for me was understanding how to actually implement the foviated rendering itself. I spent a lot of time researching the topic, but found no actual code samples for how it could be implemented. I was rather confused by the task as I thought it meant that I needed to render the scene at a high resolution to the framebuffer first and then perform blur on the texture afterwards. This would of course be a performance loss rather than gain. 

Another issue was the fact that I realized after a while that actually implementing another offscreen pass with proper synchronization within my pacman project would take too much time due to the preexisting complexity of the code. I ended up scrapping its use and replaced the project with a stripped down version of it which I luckily had prepared beforehand in the case it was needed. 
Doing so allowed me to quickly cross reference the offscreen code sample from Sascha Willems with my shadow mapping code from pacman 3D and implement an offscreen texture rendering pass fairly quickly. 

Other than wasting a lot of time due to the two issues, the rest of the implementation went rather smooth due to my pre-existing knowledge of transferring data between render passes with shadow mapping. 

## Performance difference with and without the foviated rendering
I also tried to measure the performance of the final fragment shader with and without foviated rendering by calculating frame latency:
	
	* With foveated rendering: 0.405808 ms/frame after 10 seconds
	* Without foveated rendering: 0.433784 ms/frame after 10 seconds

Measuring frame latency for performance comparisons is generally recommended compared to measuring frames per second as varying frame times makes the frame rate change rapidly. This means that there is little consistency when measuring FPS as it quickly bounces up and down without any vertical synchronization. 
In the case of this performance test, the difference isn't particularly big. This could be due to the fact that the scene is very simple as it only contains two renderable objects. Another possible reason for the not too big difference is the fact that the fragment shader uses branching to decide whether to calculate at high resolution or whether to use the low resolution texture. This branching should as far as I know provide a cost to the branch predictor's accuracy which further decreases performance. 
I could also probably do some optimizations within the vertex shader so it works similarly with the fragment shader by not performing any additional computations for vertices that would just be replaced by the rendered texture. 

## Project dependencies
Building and running the project requires several dependencies:
	
	* The first is the LunarG Vulkan SDK which can be found here: https://vulkan.lunarg.com/sdk/home#windows . I used version 1.0.46.0 specifically for this project. 
	* The next dependency is GLM which can be found here: https://github.com/g-truc/glm/tags . I used version 0.9.8.4 specifically for this project.
	* The final dependency is GLFW which can be found here: http://www.glfw.org/download.html . I used version 3.2.1 for this project specifically. 

The project also uses three environment variables that point towards their respective libraries:
	
	* "VULKAN_HOME" points to the base directory of the installation. 
	* "GLM_HOME" points to the baseDirectory\glm folder of the GLM installation. 
	* "GLFW_HOME" points to the baseDirectory\glfw-3.2.1.bin.WIN64 folder of the GLFW installation.

## Project video
I also made a short video demonstrating the program running as I do not have any portable devices that support Vulkan. The link for the video can be found here: https://www.youtube.com/watch?v=V__qfF36J0s