#pragma once
#define _USE_MATH_DEFINES
#include <memory>
#include <vector>
#include <random>
#include <glm\glm.hpp>
#include "Renderable.h"

class VulkanAPIHandler;

enum DescriptorLayoutType {
	DESC_LAYOUT_RENDERABLE = 0,
	DESC_LAYOUT_SCENE
};

class Scene {
public:
	Scene(VulkanAPIHandler* vulkanAPI);
	~Scene();

	void updateUniformBuffers(glm::mat4 projectionMatrix, glm::mat4 viewMatrix);
	void update(float deltaTime);
	void handleInput(GLFWKeyEvent event);
	
	void createTextureImages();
	void createTextureImageViews();
	void createTextureSamplers();
	void createVertexIndexBuffers();
	void createUniformBuffers();
	void createDescriptorSetLayouts();
	void createDescriptorSets(VkDescriptorPool descPool);
	void createRenderables();
	void prepareOffscreen();
	void buildOffscreenCommandBuffer();
	void prepareOffscreenPipelineLayout();
	void prepareOffscreenPipeline(VkGraphicsPipelineCreateInfo pipelineInfo);

	std::vector<std::shared_ptr<Renderable>> getRenderableObjects();
	VkDescriptorSetLayout getDescriptorSetLayout(DescriptorLayoutType type);
	VkDescriptorSet getDescriptorSet();
	VkSemaphore getOffscreenSemaphore();
	VkCommandBuffer getOffscreenCommandBuffer();
private:
	VulkanAPIHandler* vulkanAPIHandler;
	std::vector<std::shared_ptr<Renderable>> renderableObjects{};
	VDeleter<VkDevice> device;

	VkDescriptorSet sceneDescriptorSet;
	SceneUBO sceneUBO;

	OffscreenPass offscreenPass;

	VDeleter<VkDescriptorSetLayout> descriptorSetLayout{ device, vkDestroyDescriptorSetLayout };

	VDeleter<VkBuffer> uniformStagingBuffer{ device, vkDestroyBuffer };
	VDeleter<VkDeviceMemory> uniformStagingBufferMemory{ device, vkFreeMemory };
	VDeleter<VkBuffer> uniformBuffer{ device, vkDestroyBuffer };
	VDeleter<VkDeviceMemory> uniformBufferMemory{ device, vkFreeMemory };

	VDeleter<VkPipelineLayout> offscreenPipelineLayout{ device, vkDestroyPipelineLayout };
	VDeleter<VkPipeline> offscreenPipeline{ device, vkDestroyPipeline };
};